/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionSave_As;
    QAction *actionNew;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *sortButton;
    QLabel *label_4;
    QSpinBox *amountOfTeams;
    QLabel *label_2;
    QLabel *label;
    QTextBrowser *selectedStudTextBrowser;
    QPushButton *addStudentsButton;
    QListWidget *classListWidget;
    QPushButton *createBlankTeam;
    QLabel *label_3;
    QPushButton *saveTeamButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *deleteStudenButton;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(917, 666);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave_As = new QAction(MainWindow);
        actionSave_As->setObjectName(QStringLiteral("actionSave_As"));
        actionNew = new QAction(MainWindow);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(7);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        sortButton = new QPushButton(centralWidget);
        sortButton->setObjectName(QStringLiteral("sortButton"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(sortButton->sizePolicy().hasHeightForWidth());
        sortButton->setSizePolicy(sizePolicy);
        sortButton->setIconSize(QSize(20, 20));

        horizontalLayout->addWidget(sortButton);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout->addWidget(label_4);

        amountOfTeams = new QSpinBox(centralWidget);
        amountOfTeams->setObjectName(QStringLiteral("amountOfTeams"));

        horizontalLayout->addWidget(amountOfTeams);


        gridLayout->addLayout(horizontalLayout, 5, 1, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setLayoutDirection(Qt::LeftToRight);
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 3, 0, 1, 2);

        selectedStudTextBrowser = new QTextBrowser(centralWidget);
        selectedStudTextBrowser->setObjectName(QStringLiteral("selectedStudTextBrowser"));

        gridLayout->addWidget(selectedStudTextBrowser, 4, 0, 1, 2);

        addStudentsButton = new QPushButton(centralWidget);
        addStudentsButton->setObjectName(QStringLiteral("addStudentsButton"));

        gridLayout->addWidget(addStudentsButton, 5, 0, 1, 1);

        classListWidget = new QListWidget(centralWidget);
        classListWidget->setObjectName(QStringLiteral("classListWidget"));

        gridLayout->addWidget(classListWidget, 1, 0, 1, 1);

        createBlankTeam = new QPushButton(centralWidget);
        createBlankTeam->setObjectName(QStringLiteral("createBlankTeam"));

        gridLayout->addWidget(createBlankTeam, 6, 1, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_3, 0, 1, 1, 1);

        saveTeamButton = new QPushButton(centralWidget);
        saveTeamButton->setObjectName(QStringLiteral("saveTeamButton"));

        gridLayout->addWidget(saveTeamButton, 6, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 7, 0, 1, 1);

        deleteStudenButton = new QPushButton(centralWidget);
        deleteStudenButton->setObjectName(QStringLiteral("deleteStudenButton"));

        gridLayout->addWidget(deleteStudenButton, 7, 1, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 917, 25));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionNew);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0));
        actionSave->setText(QApplication::translate("MainWindow", "Save", 0));
        actionSave_As->setText(QApplication::translate("MainWindow", "Save As..", 0));
        actionNew->setText(QApplication::translate("MainWindow", "New", 0));
        sortButton->setText(QApplication::translate("MainWindow", "Sort Students", 0));
        label_4->setText(QApplication::translate("MainWindow", "Number of Teams:", 0));
        label_2->setText(QApplication::translate("MainWindow", " Class List", 0));
        label->setText(QApplication::translate("MainWindow", "Student Information", 0));
        addStudentsButton->setText(QApplication::translate("MainWindow", "Add Student(s)", 0));
        createBlankTeam->setText(QApplication::translate("MainWindow", "Create Blank Team", 0));
        label_3->setText(QApplication::translate("MainWindow", "List of Teams", 0));
        saveTeamButton->setText(QApplication::translate("MainWindow", "Save Teams", 0));
        deleteStudenButton->setText(QApplication::translate("MainWindow", "Delete Student", 0));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
