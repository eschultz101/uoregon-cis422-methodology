/**
Joey McMahon
11/9/15
*/
#include <iostream>
#ifndef HELPER_HPP_INCLUDED
#define HELPER_HPP_INCLUDED

using namespace std;

string toLowerCase(string low);
int strCompare(const string one, const string two);
bool isUpperCase(const char c);

#endif // HELPER_HPP_INCLUDED
