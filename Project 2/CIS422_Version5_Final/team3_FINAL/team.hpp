/**
Joey McMahon
11/24/15
*/

#include <iostream>
#include "student.hpp"
#ifndef TEAM_HPP_INCLUDED
#define TEAM_HPP_INCLUDED

/**
Team Class Definition

A team object, like the student object, is implemented in a doubly linked list structure. Each object contains team object
pointer variables called next and previous. Each team object contains a pointer, called head, to a linked list of students.
Each team object also contains other helpful variables for keeping track of what's going on with each team. The progSkillofChoice
variable stores the programming skill that each team's sort was based on. The highestPreferences two dimensional array of
integers stores the two highest roles preferences for each role. The preferenceNames array stores the name of the student
that has the highest preference for each role. The preferenceNames2 array stores student names as well, only it stores based on
the second highest preference. The teamSize variable contains the number of students on a team. The teamNumber variables stores
the integer that references the team object. The teamName contains the given name for each team. Names get defined in the program,
not by the user.

This class defines some functions as being only accessible through team objects. These functions are addToTeam,
removeStudentFromTeam, and printTeam. It is useful to implement them this way because then they require less to be
passed through as parameters for each function. The other functions can be called without a team object. These
functions are createTeam, findTeam, deleteStudentFromTeam, and calcTeamSkills. All of these are very useful for
accomplishing what the program needs: the ability to create teams, and store them; the ability to find a particular
team and access information about it; the ability to put a student on a team or take one off of a team; the ability
to analyze team member preferences as the customer wanted. Most of these functions, however, are not meant to be used
directly, as the class list implements functions that use these functions for help to take care similar problems that
usually need one or two more tasks to be done at the same time, such as certain object variables.
*/

class team{
//constructor and destructor
public:
team();
~team();
//Team analysis variables
int progSkillOfChoice;
int highestPreferences[numRoles][2]; //two highest role preferences
string preferenceNames[numRoles];
string preferenceNames2[numRoles];
int teamSize;
int teamNumber;
string teamName;
student *head;
team *next; //pointer to next team object.
team *previous; //pointer to the previous team object.
void addToTeam(student **headNode, student *newStud);
student *removeStudentFromTeam(student **head, student *st);
void printTeam(student *headNode);
};
team *createTeam(string teamName, int teamNumber);
team *findTeam(team *s, int teamNumber);
void deleteStudentFromTeam(team *s, int teamNumber, string lastName, string firstName);
void calcTeamSkills(team *tm);
#endif // TEAM_HPP_INCLUDED
