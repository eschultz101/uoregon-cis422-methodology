/**
Jacob Few, Joey McMahon
11/25/15
*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "student.hpp"
#include "class_list.hpp"
#include "team.hpp"
///For testing
int main()
{
    /** Test format:
    Part 1:
    create a class object
    create 4 student objects, and to save time use the same information for each one except for their name.
    You can fill in the other variables with dummy values.
    Be sure to create the following arrays and pass them through the create student function in the right place,
    along with other appropriate variables.
    int prgm_skills[5];
	int skills[13];
    student *createStudent(string lastname, string firstname, int id, int prgmSkills[5], int skills[13], int team, string othrPrgmLg);
    Then use the addStudent function ( classListName.addStudent(...) ) to add all the student objects to the class list.
    Then print the class list using classListName.printClass();
    Everyone's team number to begin with will be -1, though this might not be printed. If not, this is still correct.
    Any information you entered for each student should be displayed and should be correct. If any output looks mangled,
    let me know and record that. That will mean we need to do more dynamic memory allocation. I suspect we won't.

    Part 2 (continued off of part 1):
    create 5 more student objects the same way you created them before.
    Next we want to add the students to teams.
    To do this, we first need to create the teams. Let's make 3
    use: team *createTeam(string teamName, int teamNumber);
    Be sure to make the teamNumber different for each team. THIS IS IMPORTANT. Use different team names as well.
    Once the teams are created, add them to the classList
    Use classList::addTeam(team *origin, team *newTeam)
    origin is classListName->teams
    Now add 3 students to each team, leaving one not on a team on purpose
    To make this easy, just use the variables you used before when creating the students.
    Use the function: classList::addStudToTeam(int teamNumb, student *st).
    This will find the teams for you in the classList. Be sure to enter the correct number.
    Now let's print all the team information.
    Use classListName.printTeams(team *headNode);
    headNode: classListName->teams;
    Verify that the information printed out is correct.

    Part 3 (continued off of part 2):
    Now we are going to try removing student's from the class list. This will automatically remove them from any team that they
    are on. We won't try swapping teams yet as that function is not implemented yet.
    Use bool classList::removeStudent(student *headNode, student* stud) to remove 1 from one team, 2 from the other, and 3
    from the last one.
    headNode: classList->head
    stud: the student to be removed.
    Once you have deleted them,
    print the classNames using
    classListName.printClassNames();
    and print the team information using classListName.printTeams(team *headNode) again.
    */

    ///  SAMPLE TEST: Build on this.
/**
 Test Cases written by Jake Few Tester
 11/19/15
*/
    classList *cis422 = createClassList("CIS422");
    int prgmSkills[] = {4, 4, 4, 4, 4, 4};
    int skills[] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
    int rolePreferences[numRoles] = {1,2,3,4};
    student *s1 = createStudent("McMahon", "Joey", 1234567, prgmSkills,skills,rolePreferences, "NONE");
    cis422->addStudentToClass(s1);
    student *s2 = createStudent("Few", "Jake", 1234568, prgmSkills,skills,rolePreferences ,"NONE");
    cis422->addStudentToClass(s2);
    student *s3 = createStudent("Owens", "Andrew", 1234569, prgmSkills,skills,rolePreferences ,"NONE");
    cis422->addStudentToClass(s3);
    student *s4 = createStudent("Schultz", "Eric", 1234570, prgmSkills,skills,rolePreferences ,"NONE");
    cis422->addStudentToClass(s4);
    student *s5 = createStudent("Smith", "John", 1234571, prgmSkills,skills, rolePreferences,"NONE");
    cis422->addStudentToClass(s5);
    student *s6 = createStudent("Thompson", "Bob", 1234572, prgmSkills,skills, rolePreferences,"NONE");
    cis422->addStudentToClass(s6);
    student *s7 = createStudent("Ruiz", "Luis", 1234573, prgmSkills,skills, rolePreferences ,"NONE");
    cis422->addStudentToClass(s7);
    student *s8 = createStudent("Ritter", "Tyler", 1234574, prgmSkills,skills, rolePreferences,"NONE");
    cis422->addStudentToClass(s8);
    student *s9 = createStudent("Howe", "Austin", 1234575, prgmSkills,skills, rolePreferences ,"NONE");
    cis422->addStudentToClass(s9);
    cis422->printClass(cis422->head);
    cout << endl;
    team *t1 = createTeam("Waffles and Biscuits",0);
    cis422->addTeam(&(cis422->teams),t1); //adding team to class
    cis422->addStudentToTeam(t1->teamNumber,s1, false); //adding student to team
    cis422->addStudentToTeam(t1->teamNumber,s2, false);
    cis422->addStudentToTeam(t1->teamNumber,s3, false);
    team *t2 = createTeam("Team 1",1);
    cis422->addTeam(&(cis422->teams),t2);
    cis422->addStudentToTeam(t2->teamNumber,s4, false);
    cis422->addStudentToTeam(t2->teamNumber,s5, false);
    cis422->addStudentToTeam(t2->teamNumber,s6, false);
    team *t3 = createTeam("Team 2",2);
    cis422->addTeam(&(cis422->teams),t3);
    cis422->addStudentToTeam(t3->teamNumber,s7, false);
    cis422->addStudentToTeam(t3->teamNumber,s8, false);
    cis422->addStudentToTeam(t3->teamNumber,s9, false);
    cis422->printTeams(cis422->teams);
    cout << endl;
    calcTeamSkills(t1);
    for (int i = 0; i < numRoles; i++)
    {
        cout << "HIGHEST PREF: " << t1->highestPreferences[i][0] << " " << t1->highestPreferences[i][1] << endl;
        cout << "Names: " << t1->preferenceNames[i] << ", " << t1->preferenceNames2[i] << endl << endl;
    }
    cis422->sortTeams(3);
    cout << "TEAMS DELETED THEN SORTED AGAIN" << endl;
    cis422->printTeams(cis422->teams);
    cis422->deleteStudentOnTeam(s1);
    cis422->deleteStudentOnTeam(s2);
    cout << endl << "STUDENTS S1 and S2 deleted." << endl;
    cis422->printTeams(cis422->teams);
    cout << endl;
    cis422->swapStudentTeamToClass(s3);
    cout << "SWAPPING STUDENT S3 BACK TO CLASS" << endl;
    cis422->printClassNames(cis422->head);
    cis422->deleteStudentFromClass(&s3);
    cout << endl;
    cout << "REMOVING STUDENT S3 FROM CLASS" << endl;
    cis422->printClassNames(cis422->head);
    cout << endl;
    cis422->removeTeamFromClass(cis422->teams);
    cout << "TEAM 0 REMOVED" << endl << endl;
    cis422->printTeams(cis422->teams);
    cout << endl;
    cis422->swapStudentTeamToTeam(s4,s7->team);
    cis422->swapStudentTeamToTeam(s5,s7->team);
    cout << "SWAPPING ERIC SCHULTZ AND JOHN SMITH TO TEAM 1" << endl << endl;
    cis422->printTeams(cis422->teams);
    cout << endl;
    freeClassList(cis422);
    cout << "CLASS LIST FREED COMPLETELY" << endl << endl;
    cis422->printClass(cis422->head);
    cout << endl;
    cis422->printTeams(cis422->teams);
    cout << endl;
    //cis422->printTeams(cis422->teams);
    cout << endl;
    return 0;
}

