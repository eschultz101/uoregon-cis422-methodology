/**
Eric Schultz
11-5-2015
Joey McMahon
11-28-15
*/

#include <iostream>
#include "student.hpp"
#include "team.hpp"

#ifndef CLASS_LIST_HPP_INCLUDED
#define CLASS_LIST_HPP_INCLUDED

/**
This is the most important class for the program. The main design idea was to keep everything in one place:
this class object. It contains the student linked list and all existing teams in a linked list as well. It
contains useful variables for sorting teams such as the number of students and an array which stores for each
programming skill the number of students who rated this skill the highest. The class is designed to take
care of all the important functionality with relative ease. This required that many functions implemented
here use functions from other classes such as the student class or, primarily, from the team class. The
difference between these functions is in the ease of using the ones in this class. The implementation in
this class uses the student object as a parameter primarily when it comes to moving student objects from
student list to team, from one team to another, and from team to student list; whereas in the team class the
functions use linked lists of students along with a specific student.

There were other specific design decisions made. When one puts a student on a team, this does not take away
from the total number of students in a class. Thus, when one adds a student to back to the class list from a team,
the program does not add to the total number of students. This number is only updated for new students and when a
student is removed and deleted from the class list or from a team. This makes sense since students on teams are still
apart of the class. From a programming perspective, this allows the program to know how many students there are before
sorting students into teams. This design is convenient since if students are already on teams, the program will still
know the total number when sorting, which it uses since it removes all moves all students from teams back to student
list first.
*/

class classList{
    public:

    // Constructor and destructor
    classList();
    ~classList();

    string className;
	int numStudents; // Number of students in list
	int numTeams; //number of teams.
	int numBestSkills[numProgSkills]; //for keeping track of the available best programming skills among remaining students that are
                          //not on teams.
	student* head; 	//Head of this linked list of students
    team *teams; //Head of this linked list of teams

    ///Functions: Named after their use. Not fully described here. See class_list.cpp for more information,
	void addStudentToClass(student *st); //add a student to a the linked list of students in the class list.
	int addStudent(student **headNode, student *st); //helper function for addStudentToClass
	student *removeStudentFromClass(student **headNode, student* st); //remove a student object for further use.
	void deleteStudentFromClass(student **st); //completely get rid of a student object.
	void freeStudentList(student **st); //get rid of an entire list of students using any student object in the list.
    void printClass(student *headNode); //prints all student names in order and their information.
    void printClassNames(student *headNode); //prints names of all students in class list in order.
    void addStudentToTeam(int teamNumb, student *st, bool updateTeamSkills); //adds a student to a particular team
    void freeTeams(team **s); //frees all teams in a linked list of teams, given the head pointer.
    void freeTeam(team **t); //frees a single team. Helper function for freeTeams.
    void removeTeamFromClass(team *tm); //deletes a team and its students from a class list.
    void addTeam(team **origin, team *newTeam); //adds a team to a linked list of teams.
    void printTeams(team *head); //prints team name, team size, and team members.
    void sortTeams(int numTeams); //sorts all students currently in class list (including students on teams) into teams.
    void swapStudentTeamToClass(student *st); //moves a student from a team back to the student list in the class list.
    void deleteStudentOnTeam(student *st); //completely removes a student that is on a team.
    void swapStudentTeamToTeam(student *st, int otherTeam); //moves a student from their current team to another one.
    private:
    int chooseStudents(team *tm, int studPerTeam); //helper function for sort.
    void updateClassSkills(student *st, bool add); //updates numBestSkills
    student *findSkillMax(student *st, int skill, int topRating); //helper function for sort.
    student *mustGetStudent(student *head, int mostBestSkill); //helper function for sort.
    void returnAllStudentsFromTeams(team **head); //helper function for sort.
    void returnStudentsFromTeam(team **tm); //helper function for returnAllStudentsFromTeams.
};
student *findStudentBySkill(student *head, int mostBestSkill, bool onlyOneBest); //helper function for sort.
classList *createClassList(string className); //creates and initializes a class list object
void freeClassList(classList *cL); //completely frees a class list object and all its contents.


#endif // CLASS_LIST_HPP_INCLUDED
