CXX = g++
CXXFLAGS = -g -std=c++11

%.o : %.c
	$(CXX) -c $< $(CXXFLAGS)

SOURCES = student.cpp class_list.cpp team.cpp helper.cpp main.cpp
HEADERS = student.hpp class_list.hpp team.hpp helper.hpp
OBJECTS = student.o class_list.o team.o helper.o main.o
LIBS = -lm

main: $(OBJECTS)
	$(CXX) -o $@ $(OBJECTS) $(CXXFLAGS)

clean:
	$(RM) main $(OBJECTS)
