/**
Eric Schultz
11-5-2015
Joey McMahon
11-9-15
11-14-15
*/

#include <iostream>
#include "student.hpp"
#include "team.hpp"

#ifndef CLASS_LIST_HPP_INCLUDED
#define CLASS_LIST_HPP_INCLUDED

class classList{
    public:

    // Constructor and destructor
    classList();
    ~classList();

    string className;
	int numStudents;
	// Number of students in list
	int numTeams;
	// Number of teams
	student* head;
	// First student in class list
	//student* tail;
    //last student in class list.
    team *teams;
	// Removes student from class list
	bool removeStudent(student **headNode, student* st);
	// Adds student to class list.
    void addStudent(student **headNode, student *st);
    //function for finding and returning a particular student from a class list.
    student *findStudent(student *head, string firstName, string lastName);
    ///prints all student names in order and their information.
    void printClass(student *headNode);
    ///prints names of all students in class list in order.
    void printClassNames(student *headNode);
    ///function for creating a stud object and then adding it to a particular team.
    void addStudToTeam(int teamNumb, student *st);
    void freeTeams(team **s);
    void freeTeam(team **t);
    void addTeam(team **origin, team *newTeam);
    void printTeams(team *head);
};

classList *createClassList(string className);
void freeClassList(classList *cL);
#endif // CLASS_LIST_HPP_INCLUDED
