/**
Joey McMahon
11/14/15
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "helper.hpp"
#include "team.hpp"

using namespace std;
///Constructor and Destructor for team objects
team::team(){}
team::~team(){}
/**
Creates a blank new team and initializes team size to zero, with pointer to next team at null.
teamName: desired team name. Should be system defined, not user.
teamNumber: Should be based off of teamName (i.e., team0's teamNumber is 0)
*/
team *createTeam(string teamName, int teamNumber){
    team *newTeam = new team;
    newTeam->teamName = teamName;
    newTeam->teamNumber = teamNumber;
    newTeam->teamSize = 0;
    newTeam->StudentHead = NULL;
    newTeam->next = NULL;
    return newTeam;
}

/**
headNode: pointer to the front of the linked list of student names in a team.
newStud: pointer to new stud node (student object only containing name)
Only use this function to add somebody to a team if they are not already on one.
*/
void team::addToTeam(stud **headNode, stud *newStud){
 if (*headNode == NULL)
 {
     (*headNode) = newStud;
     (this->teamSize)++;
     return;
 }
 int comp = strCompare((*headNode)->lastName, newStud->lastName);
    if (comp == 0) {
    	comp = strCompare((*headNode)->firstName, newStud->firstName);
    }
    if (comp > 0) // head node's name comes first. Add after.
    {
        if ((*headNode)->next == NULL) //if next is null, add it.
        {
			(*headNode)->next = newStud;//student added.
			newStud->previous = (*headNode);
			(this->teamSize)++;
        }
        else
        {
            this->addToTeam(&((*headNode)->next), newStud);
        }
    }
    else if (comp < 0)// (*headNode) comes second. Add student before it.
    {
        if ((*headNode) == (this->StudentHead)){ //replacing head
			StudentHead->previous = newStud; //point head's previous to new node
			newStud->next = StudentHead;  //point new node to head
			StudentHead = newStud;           //update head node.
        }
        else //insert before (*headNode)
        {
			stud *temp = (*headNode); //save (*headNode)
			(*headNode)->previous->next = newStud; //connect previous node to stud
			newStud->next = temp; //connect stud to rest of list
			newStud->previous = (*headNode)->previous;
			(*headNode)->previous = newStud;
        }
        (this->teamSize)++;
    }
    else // Names are identical. Do nothing
    {

    }
}
/**
removes a stud object from a team using that team's linked list.
Thus headNode should always be the StudentHead variable found in each team.
headNode: the head of the linked list of stud objects in a team object
studRem: the stud object to be removed from the list.
*/
stud *team::removeFromTeam(stud **headNode, stud *studRem){
    if ((*headNode) == NULL)
    {
        return NULL;
    }
    int comp = strCompare((*headNode)->lastName, studRem->lastName);
    if (comp == 0) {
    	comp = strCompare((*headNode)->firstName, studRem->firstName);
    }
    if (comp > 0) // head node's name comes first. Search after.
    {
       return  (this->removeFromTeam(&((*headNode)->next),studRem));
       //return del;
    }
    else if (comp < 0)// (*headNode) comes second. But we already searched previous names.
    {
       return NULL;
    }
    else // Names are identical. Remove node
    {
        if (this->StudentHead == (*headNode))
        {
            if (((*headNode)->next) != NULL)
            {
                StudentHead = (*headNode)->next;
                (*headNode)->next = NULL;
                this->teamSize--;
                return (*headNode);
            }
            else
            {
                StudentHead = NULL;
                this->teamSize--;
                return (*headNode);
            }
        }
        else if ((*headNode)->next == NULL) //node is at end of list.
        {
            (*headNode)->previous->next = NULL;
            (*headNode)->previous = NULL;
            this->teamSize--;
            return (*headNode);
        }
        else //node to be deleted is in between two nodes.
        {
            (*headNode)->previous->next = (*headNode)->next;
            (*headNode)->next->previous = (*headNode)->previous;
            (*headNode)->next = NULL;
            (*headNode)->previous = NULL;
            this->teamSize--;
            return (*headNode);
        }
    }
}

/**
Finds a team based on team number. This is useful since complete student objects have this piece of information.
s: a list of teams to be searched
teamNumber: team identification number, found in all team objects and complete student objects.
Returns pointer to the desired team if found; otherwise, returns NULL
*/
team *findTeam(team *s, int teamNumber){
    if (s == NULL)
    {
        return NULL;
    }
    else
    {
        if (s->teamNumber == teamNumber)
        {
            return s;
        }
        else
        {
            if (s->next != NULL)
            {
            return findTeam(s->next, teamNumber);
            }
            else //no more options.
            {
            return NULL;
            }
        }
    }
}
/**
Corollary to findTeam
Finds a particular stud object in a stud list within a team.
Needed for when we remove a student from the class list.
head: pointer to the first stud object in team where the student can be found.
lastName: last name of student
firstName: first name of student
returns pointer to this stub object if found; Otherwise, returns null.
*/
stud *findStud(stud *head, string lastName, string firstName){
     if (head == NULL)
     {
         return NULL;
     }
     int comp = strCompare((head->lastName), lastName);
     if (comp == 0){
     comp = strCompare(head->firstName, firstName);
     }
     if (comp == 0)
     {
         return head;
     }
     else if (comp < 0) //student's name comes alphabetically first but we have already checked previous names
     {
         return NULL;
     }
     else //comp > 0
     {
         return (findStud(head->next,firstName,lastName));
     }
}

/**
Removes a stud object from a specific team, searching the entire team list from a classList to do so.
This function is called inside the removeStudent function so that when one removes a student from the class list,
he/she is automatically removed from whatever team he/she was on.
This function could also be used individually if desired, though it should not be used if someone wants to switch
a student from one team to another.
s: a list of team objects
teamNumber: Should come from the student's int team variable.
lasname/firstname: the student's names
*/
void removeStud(team **s, int teamNumber, string lastName, string firstName){
if (*s == NULL)
{
    return;
}
if (teamNumber == -1) //Student is not on a team
{
    return;
}
team *t = findTeam(*s,teamNumber);
if (t == NULL)
{
    return;
}
stud *st = findStud(t->StudentHead,lastName,firstName);
if (st == NULL)
{
    return;
}
t->removeFromTeam(&(t->StudentHead),st);
freeStud(&st);
}


void team::printTeam(stud *headNode){
if (headNode == NULL)
{
    return;
}
cout << headNode->firstName << " " << headNode->lastName << endl;
if (headNode->next != NULL)
{
this->printTeam(headNode->next);
}
}
