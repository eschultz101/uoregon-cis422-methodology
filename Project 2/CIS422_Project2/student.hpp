/** Eric Schultz
    11/8/15
    Joey McMahon
    11/9/15
    11/14/15
    Student class and functions header
*/

#include <iostream>
#ifndef STUDENT_HPP_INCLUDED
#define STUDENT_HPP_INCLUDED

using namespace std;
/* Implementation of student class

We are using a class instead of a struct for the class list because of the relatively large amount of information stored in each student object.
Most important fields included thus far.
Each student has a pointer to the next student. Thus we are using a linked list.
We are using a struct for the team lists because we will only need student names and linkers in this data structure.
*/

struct stud{
string lastName;
string firstName;
stud *next;
stud *previous;
};

using namespace std;
class student {
    public:
    //constructor and destructor.
    //necessary for class. Don't really use these.
    student();
    ~student();
	// Basic information
	string lastName;
	string firstName;
	string wholeName;
	int student_id;
	//string email;
	int prgm_skills[5];
	// skills[0] indicates proficiency in c, skills[1] in c++, skills[2] in java, 3 = python, 4 = web design
	int skills[13];
	/*
	skills[i]: 0: problem solving, 1: Analytic thinking, 2: Thinking up new ideas, 3: Public speaking, 4: Planning/organizing,
	5: good with big picture, 6: Powerpoint, 7: team building, 8: good with details, 9: spoken English, 10: Written English,
	11: Technical Writing, 12: Outgoing (5), shy (1)
	Rating of zero, means no skill, 5 means high proficiency.
	*/

	string other_prgm;
	// Extra space for students to write in other programming languages they know

	string prof_exp;
	// Space for students to describe any professional programming experience they may have

	char avg_grade[2];
	// The student's average grade in CIS classes (I figure this is better than asking for grades they got in each class?)

	int team;
	// Indicates what team this student is on, if any

	student *next;
	student *previous;
	// Pointer to next student in list/team (TODO: studentList or team??)

	void printInfo();

};

// Student class helper functions:
///Function for creating complete student object with all required fields.
student *createStudent(string lastname, string firstname, int id, int prgmSkills[5], int skills[13], string othrPrgmLg);
///Function for creating simpler student object for teams.
stud *createTeamMember(string lastName, string firstName);

void freeStudent(student **st);
void freeStud(stud **s);
void printStud(stud *s);
#endif // STUDENT_HPP_INCLUDED
