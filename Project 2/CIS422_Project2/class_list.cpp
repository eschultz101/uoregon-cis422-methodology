/*
Eric Schultz
11-5-15
Joey McMahon
11-9-15
11/14/15
*/

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "class_list.hpp"
#include "helper.hpp"
#include "team.hpp"

using namespace std;

///Constructor and Destructor for class objects
classList::classList(){}
classList::~classList(){}

/**
    Function for adding a student to the current linked list of students in a class list
	*headNode: the head node of the class list
	*stud: the student to be added to the class list
*/
void classList::addStudent(student **headNode, student *st) {
    if ((*headNode) == NULL)
    {
        (*headNode) = st;
        return;
    }
   int comp = strCompare((*headNode)->lastName, st->lastName);
    if (comp == 0) {
    	comp = strCompare((*headNode)->firstName, st->firstName);
    }
    if (comp > 0) // head node's name comes first. Add after.
    {
        if ((*headNode)->next == NULL) //if next is null, add it.
        {
			(*headNode)->next = st; //student added.
			st->previous = (*headNode);
			(this->numStudents)++;
        }
        else
        {
            addStudent(&((*headNode)->next), st);
        }
    }
    else if (comp < 0)// (*headNode) comes second. Add student before it.
    {
        if ((*headNode) == this->head){ //replacing head
			student *temp = this->head; //save the head
			this->head = st;  //change head to be pointing at added student
			st->next = temp; //connect student to old head.
			temp->previous = st; //connect head to stud
        }
        else //insert before (*headNode)
        {
			(*headNode)->previous->next = st; //connect previous node to stud
			st->next = (*headNode); //connect stud to rest of list
			st->previous = (*headNode)->previous;
			(*headNode)->previous = st;
        }
        (this->numStudents)++;
    }
    else // Names are identical. User is probably updating student info.
    {
        st->next = (*headNode)->next; //replace node by using its pointers and then freeing it.
        st->previous = (*headNode)->previous;
        if (this->head == (*headNode))
        {
            this->head = st;
        }
    freeStudent(&(*headNode));
    }
}

/**
This function links team objects together.
This function is meant to be used for classList object's teams variable which stores all the teams of a given class list.
origin: the head pointer to the linked list of teams in a classList object.
newTeam: the team to be added to the list.
*/
void classList::addTeam(team **origin, team *newTeam){
    if ((*origin) == NULL)
    {
        (*origin) = newTeam;
        this->numTeams++;
        return;
    }
    else if ((*origin)->next == NULL)
    {
        (*origin)->next = newTeam;
        this->numTeams++;
    }
    else
    {
    addTeam(&((*origin)->next), newTeam);
    }
}

/** removeStudent
    *headNode: the head node of the class linked list
    *stud: the student to be removed.
    I suggest implementing in a very similar way to how I implemented add.
    Only when you find the right node, remove it. If there's nodes on both sides, connect them.
    If you delete the head node, update the head node if possible. Same with the tail.
*/
bool classList::removeStudent(student **headNode, student* st){
    int comp = strCompare((*headNode)->lastName, st->lastName);
    if (comp == 0) {
    	comp = strCompare((*headNode)->firstName, st->firstName);
    }
    if (comp > 0) // head node's name comes first. Search after.
    {
       return (this->removeStudent(&((*headNode)->next),st));
    }
    else if (comp < 0)// (*headNode) comes second. But we already searched previous names.
    {
       return false;
    }
    else // Names are identical. Remove node
    {
        if (this->head == (*headNode))
        {
            if (((*headNode)->next) != NULL)
            {
                head = (*headNode)->next;
                removeStud(&(this->teams),(*headNode)->team,(*headNode)->lastName,(*headNode)->firstName);
                freeStudent(&(*headNode));
                this->numStudents--;
                return true;
            }
            else
            {
                removeStud(&(this->teams),(*headNode)->team,(*headNode)->lastName,(*headNode)->firstName);
                freeStudent(&(*headNode));
                this->numStudents--;
                return true;
            }
        }
        else if ((*headNode)->next == NULL) //node is at end of list.
        {
            (*headNode)->previous->next = NULL;
            removeStud(&(this->teams),(*headNode)->team,(*headNode)->lastName,(*headNode)->firstName);
            freeStudent(&(*headNode));
            this->numStudents--;
            return true;
        }
        else //node to be deleted is in between two nodes.
        {
            (*headNode)->previous->next = (*headNode)->next;
            (*headNode)->next->previous = (*headNode)->previous;
            removeStud(&(this->teams),(*headNode)->team,(*headNode)->lastName,(*headNode)->firstName);
            freeStudent(&(*headNode));
            this->numStudents--;
            return true;
        }
    }
}

/**
Function for creating and initializing a new classList object.
className: name of the class
*/
classList *createClassList(string className) {

	classList *newClass = new classList();
	// Initialize classList values
	newClass->className = className;
	newClass->numStudents = 0;
	newClass->numTeams = 0;
	newClass->head = NULL;
	newClass->teams = NULL;
	return newClass;
}
 /**
 This function finds a student based on name in the current class list object and returns it if it is found.
 Returns NULL otherwise.
 head: pointer to head of the linked list of students in a class list.
 firstName/lastName: name of desired student to be found.
 */
 student *classList::findStudent(student *head, string firstName, string lastName){
     int comp = strCompare((head->lastName), lastName);
     if (comp == 0){
     comp = strCompare(head->firstName, firstName);
     }
     if (comp == 0)
     {
         return head;
     }
     else if (comp < 0) //student's name comes alphabetically first but we have already checked previous names
     {
         return NULL;
     }
     else //comp > 0
     {
         return (this->findStudent(head->next,firstName,lastName));
     }
}

/**
Prints all students names and all their information to the console.
Must be called on a class list object
*/
void classList::printClass(student *headNode){
    if (headNode == this->head)
    {
    cout << this->className << endl;
    }
    if (headNode != NULL)
    {
        headNode->printInfo();
    }
    if (headNode->next != NULL)
    {
        this->printClass(headNode->next);
    }
    else
    {

    }
}

/**
Prints only student names of a class in order.
Must be called on a classList object.
*/
void classList::printClassNames(student *headNode){
    cout << this->className << endl;
    if (headNode != NULL)
    {
        cout << headNode->firstName << " " << headNode->lastName << endl;
    }
    else if (headNode == NULL)
    {
        cout << "NO STUDENTS" << endl;
        return;
    }
    if (headNode->next != NULL)
    {
        this->printClass(headNode->next);
    }
}
/**
This function first creates a stud object based on the student object.
Again, stud objects only contain student names and pointers to the next and previous stud object
Since we already have a function that adds a stud object to the correct place in a team, this function
is simply meant for taking away the step of having to create stud objects one's self each time one wants to add a student
to a team. This also finds the team based on the teamNumb entered.
Changes student team integer identification appropriately.
teamNumb: a number corresponding to a team that should be in the class list first.
st: the student to be added to a team.
*/
void classList::addStudToTeam(int teamNumb, student *st){
team *s = findTeam(this->teams,teamNumb);
cout << s->teamName << endl;
///NOTE: TEAM SIZE UPDATED IN addToTeam. DO NOT DO IT HERE.
stud *newStud = createTeamMember(st->lastName, st->firstName);//creating stud object
s->addToTeam(&(s->StudentHead),newStud); //add the stud object to the team.
st->team = teamNumb;
//updateTeamStats(s,st);
}

/**
Frees a single team object's memory by freeing each stud object from the team's stud list one by one.
t: pointer to pointer to team object. Need double pointer since we are modifying a pointer to a team object.
*/
void classList::freeTeam(team **t){
    stud *temp = (*t)->removeFromTeam(&((*t)->StudentHead),(*t)->StudentHead);
    while (temp != NULL)
    {
    freeStud(&temp);
    //I believe removing the head first is the fastest way to delete all the stud objects.
    temp = (*t)->removeFromTeam(&((*t)->StudentHead),(*t)->StudentHead);
    }
    (*t)->StudentHead = NULL;
    *t = NULL;
    this->numTeams--;
    free(*t);
}


/**
Frees all teams in a linked list of teams.
Something we need when the user wants to shut the app down.
s: double pointer to team object since we are modifying a pointer to a team object
*/
void classList::freeTeams(team **s){
    while (s != NULL)
    {
       team *a = *s; //save the team
       *s = (*s)->next; //move down the linked list.
       freeTeam(&a); //free the old head node.
    }
    (*s)->next = NULL;
    *s = NULL;
    free(*s);
}

 /**
 prints all team names and member names.
 */
 void classList::printTeams(team *headNode){
     if (headNode != NULL)
     {
     cout << "TeamName: " << headNode->teamName << endl;
     cout << "TeamNumber: " << headNode->teamNumber << endl;
     cout << "TeamSize: " << headNode->teamSize << endl;
     cout << "Team Members:" << endl;
     }
     if (headNode->StudentHead == NULL)
     {
         cout << "EMPTY TEAM" << endl;
     }
     else
     {
     headNode->printTeam(headNode->StudentHead);
     }
     if (headNode->next != NULL)
     {
     this->printTeams(headNode->next);
     }
 }
