/** Eric Schultz
 11-8-15
 Joey McMahon
 11/9/15
 11-14-2015
 Student function implementation
 */

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "student.hpp"
#include "helper.hpp"

using namespace std;

///Constructor and Destructor for student objects
student::student(){}
student::~student(){}

/**
prints all of a student's information.
*/
void student::printInfo() {
	cout << this->lastName << ", " << this->firstName << "\n";
	cout << this->student_id << "\n";
	cout << "Student programming skills:\n" << "C: " << this->prgm_skills[0] << "  C++: " << this->prgm_skills[1]
			<< "  Java: " << this->prgm_skills[2] << "  Python: " << this->prgm_skills[3] << "  Web design: "
				<< this->prgm_skills[4] << "\n";
	if((this->other_prgm).length() > 0) cout << "Other student programming languages: " << this->other_prgm << "\n";
	if((this->prof_exp).length() > 0) cout << "Student's professional experience: " << this->prof_exp << "\n";
	cout << "Skills: ";
	for (int i = 0; i < 13; i++)
    {
        cout << this->skills[i] << " ";
    }
    cout << endl;
	//cout << "Student's average grade in CIS classes: " << this->avg_grade << "\n";
	if(team != -1) cout << "Student is on team " << this->team << "\n";
}

/*
Create Student
Assumption: Skills are integers between 0 and 5.
This will need to be checked elsewhere unless we decide to do it here. I suggest not doing it here since
then we would need a bunch of user interactivity here which would clutter this big time.
last name: student's lastname
first name: student's firstname
id: student id
prgmSkills[5]: the integer rated program language skills formatted in the correct order see student.hpp
int skills[13]; the integer rated personal skills formatted in the correct order: see student.hpp
team: which team if any the student belongs to. Most likely will not belong to a team at first. Thus this should be entered as a
negative number to begin with.
othrPrgmLg: other programming language the student may have familiarity with.
*/
student *createStudent(string lastname, string firstname, int id, int prgmSkills[5], int skills[13], string othrPrgmLg){

	student *s = new student;
    s->lastName = lastname;
    s->firstName = firstname;
    s->wholeName = firstname + lastname;
    s->student_id = id;
    for (int i = 0; i < 5; i++){
    	s->prgm_skills[i] = prgmSkills[i];
    }
    for (int j = 0; j < 13; j++){
        s->skills[j] = skills[j];
    }
    s->team = -1; //currently not on a team.
    s->other_prgm = othrPrgmLg;
    s->next = NULL;
    s->previous = NULL;
    return s;
}

/**
Creates a student object meant for being used in teams objects.
Makes it so updates to student info besides name don't need to be updated in two different places.
Also will help save memory, since now we won't need two complete student objects for the program.
studName: student's name.
*/
stud *createTeamMember(string lastName, string firstName){
stud *newStud = new stud;
newStud->lastName = lastName;
newStud->firstName = firstName;
newStud->next = NULL;
newStud->previous = NULL;
return newStud;
}

/**
frees a student object from memory
st: double pointer to student object to be deleted.
*/
void freeStudent(student **st){
(*st)->next = NULL;
(*st)->previous = NULL;
*st = NULL;
free((*st));
}
/**
Frees a stud object
s: double pointer to student object since we are modifying a pointer to a stud object
*/
void freeStud(stud **s){
(*s)->next = NULL;
(*s)->previous = NULL;
free(*s);
}

void printStud(stud *s){
cout << s->firstName << " " << s->lastName << endl;
}
