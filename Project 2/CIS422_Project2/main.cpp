/**
Joey McMahon
11/9/15
*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "student.hpp"
#include "class_list.hpp"
#include "team.hpp"
///For testing
int main()
{
    /** Test format:
    Part 1:
    create a class object
    create 4 student objects, and to save time use the same information for each one except for their name.
    You can fill in the other variables with dummy values.
    Be sure to create the following arrays and pass them through the create student function in the right place,
    along with other appropriate variables.
    int prgm_skills[5];
	int skills[13];
    student *createStudent(string lastname, string firstname, int id, int prgmSkills[5], int skills[13], int team, string othrPrgmLg);
    Then use the addStudent function ( classListName.addStudent(...) ) to add all the student objects to the class list.
    Then print the class list using classListName.printClass();
    Everyone's team number to begin with will be -1, though this might not be printed. If not, this is still correct.
    Any information you entered for each student should be displayed and should be correct. If any output looks mangled,
    let me know and record that. That will mean we need to do more dynamic memory allocation. I suspect we won't.

    Part 2 (continued off of part 1):
    create 5 more student objects the same way you created them before.
    Next we want to add the students to teams.
    To do this, we first need to create the teams. Let's make 3
    use: team *createTeam(string teamName, int teamNumber);
    Be sure to make the teamNumber different for each team. THIS IS IMPORTANT. Use different team names as well.
    Once the teams are created, add them to the classList
    Use classList::addTeam(team *origin, team *newTeam)
    origin is classListName->teams
    Now add 3 students to each team, leaving one not on a team on purpose
    To make this easy, just use the variables you used before when creating the students.
    Use the function: classList::addStudToTeam(int teamNumb, student *st).
    This will find the teams for you in the classList. Be sure to enter the correct number.
    Now let's print all the team information.
    Use classListName.printTeams(team *headNode);
    headNode: classListName->teams;
    Verify that the information printed out is correct.

    Part 3 (continued off of part 2):
    Now we are going to try removing student's from the class list. This will automatically remove them from any team that they
    are on. We won't try swapping teams yet as that function is not implemented yet.
    Use bool classList::removeStudent(student *headNode, student* stud) to remove 1 from one team, 2 from the other, and 3
    from the last one.
    headNode: classList->head
    stud: the student to be removed.
    Once you have deleted them,
    print the classNames using
    classListName.printClassNames();
    and print the team information using classListName.printTeams(team *headNode) again.
    */

    ///  SAMPLE TEST: Build on this.

    classList *cis422 = createClassList("CIS422");
    int prgmSkills[] = {4, 4, 4, 4, 4};
    int skills[] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
    student *s1 = createStudent("McMahon", "Joey", 1234567, prgmSkills,skills, "NONE");
    cis422->addStudent(&(cis422->head), s1);
    cis422->printClass(cis422->head);
    cout << endl;
    team *t1 = createTeam("Waffles and Biscuits",0);
    cis422->addTeam(&(cis422->teams),t1); //adding team to class
    cis422->addStudToTeam(t1->teamNumber,s1); //adding student to team
    cis422->printTeams(cis422->teams);
    cout << endl;
    cis422->removeStudent(&(cis422->head), s1);
    cis422->printClassNames(cis422->head);
    cout << endl;
    cis422->printTeams(cis422->teams);
    cout << endl;
    return 0;
}

