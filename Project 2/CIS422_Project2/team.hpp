/**
Joey McMahon
11/14/15

Team objects are implemented as a linked list as well. Within each one is a linked list of stud objects that only
include each student's name. This is so that we do not need to create two complete student objects for our program.
This is necessary because one student object is not enough if the students are in two linked lists; pointers would get
mixed up. This also has the benefit of not having to update student info if a student's information is changed in the class list,
so long as it does not involve a change to the student's name.
*/
#include <iostream>
#include "student.hpp"
#ifndef TEAM_HPP_INCLUDED
#define TEAM_HPP_INCLUDED

class team{
//constructor and destructor
public:
team();
~team();
int teamSize;
int teamNumber;
string teamName;
stud *StudentHead; //linked list of team names
team *next; //pointer to next team object.
void addToTeam(stud **headNode, stud *newStud); //passing full student object so that in the
                                           //future this function can update team stats as well.
stud *removeFromTeam(stud **head, stud *studRem);
void updateTeamStats(student *stud, bool add); //future helper function for addToTeam and removeFromTeam
void printTeam(stud *headNode);
};
team *createTeam(string teamName, int teamNumber);
team *findTeam(team *s, int teamNumber);
stud *findStud(team *s, string lastName, string firstName);
void removeStud(team **s, int teamNumber, string lastName, string firstName);
#endif // TEAM_HPP_INCLUDED
